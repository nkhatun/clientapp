import { Component, OnInit } from '@angular/core';
import { ResponsiveService } from "../responsive.service";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
isHomeSelected:boolean=false;
isMobile:boolean=false;

  constructor(private responsiveService: ResponsiveService) { }

  ngOnInit(): void {
            this.responsiveService.getSelectedPath().subscribe(selectedPath => {
        this.isHomeSelected = selectedPath;
    });
    this.responsiveService.getMobileStatus().subscribe(isMobile => {
        this.isMobile = isMobile;
    });
  }

}
