import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
// import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { CommonService } from "../common.service";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
regForm: FormGroup;
  countryArr =[];
  addClientUrl:string;
  updateClientUrl:string;
  deleteClientUrl:string;
  listClientUrl:string;

  constructor(private fb: FormBuilder, private commonService: CommonService) {  }
columnHeader:any=[];
  ngOnInit(): void {
    this.addClientUrl="http://localhost:8080/client/api/v1/addClient";
    this.updateClientUrl="http://localhost:8080/client/api/v1/updateClient";
    this.listClientUrl="http://localhost:8080/client/api/v1/fetchAllClients";
    this.deleteClientUrl="http://localhost:8080/client/api/v1/deleteClient?clientId=";


    this.countryArr = [
    {value: 0, label: 'Country 0'},
    {value: 1, label: 'Country 1'},
    {value: 2, label: 'Country 2'},
    {value: 3, label: 'Country 3 '}
];
this.columnHeader = [{"name":"S"}];
 this.regForm = this.fb.group({
            fname: [, [Validators.required,]],
            lname: [, [Validators.required]],
            dob: [, [Validators.required]],
            country: [, [Validators.required]],
            mobile: [, [Validators.required,Validators.maxLength(10)]],
  });
this.fetchAllClients();
  }
   onSubmit(){
    console.log("reg",this.regForm);
    if(this.regForm.valid){
      if(!this.isValidMobile(this.regForm.value["mobile"])){  
            window.alert("Mobile Number Should Be 10 Digits Number Only");
     }
     else{
       var respObj = {"first_name":this.regForm.value["fname"],
       "last_name":this.regForm.value["lname"],"dob":this.regForm.value["dob"],
       "country_id":this.regForm.value["country"],"phone_no":this.regForm.value["mobile"]};
        this.commonService.postData(this.addClientUrl,respObj).subscribe(
      res => {
        this.showTable = true;
        if(res.status == 'success'){
          window.alert("Client Registered Successfully")
          this.fetchAllClients();
        }
        else{
          window.alert("Some problem occured while client registration")
        }
      });

     }
    }
    else{
      window.alert("Please fill all the required fields");
    }
  }
clientList:any=[];

fetchAllClients(){
   this.commonService.getData(this.listClientUrl).subscribe(
      res => {
        // if(res.data.status == 'success'){
        // }
        this.clientList = res;
        console.log("clientList",this.clientList);
      });
}
  isValidMobile(mobileNo){
    const pattern=/^[0-9]{10}$/;
    if(mobileNo != null && mobileNo != ""){
      if(!pattern.test(mobileNo)){
        return false;
      }
      return true;
    }
    return false;

  }

onDelete(id){
if(window.confirm("Are you sure to delete the client details?")){
   this.commonService.getData(this.deleteClientUrl+id).subscribe(
      res => {
        if(res.status == 'success'){
          this.fetchAllClients();
        }
         window.alert(res.message);
      });
}
}

onReset(){
  this.regForm.get("fname").setValue('');
this.regForm.get("lname").setValue('');
this.regForm.get("dob").setValue('');
this.regForm.get("mobile").setValue('');
this.regForm.get("country").setValue('');
}
showTable:boolean= true;
showAddButton:boolean= true;
showUpdateButton:boolean= false;
client_id:string;
onEdit(arr){
  this.showTable = false;
console.log("arr",arr);
this.regForm.get("fname").setValue(arr.first_name);
this.regForm.get("lname").setValue(arr.last_name);
this.regForm.get("dob").setValue(arr.dob);
this.regForm.get("mobile").setValue(arr.phone_no);
this.regForm.get("country").setValue(arr.country_id);
this.showAddButton= false;
this.showUpdateButton= true;
this.client_id = arr.id;
//this.onUpadte(respObj)
;
}
onUpadte(){
  var respObj = {"id":this.client_id,"first_name":this.regForm.value["fname"],
       "last_name":this.regForm.value["lname"],"dob":this.regForm.value["dob"],
       "country_id":this.regForm.value["country"],"phone_no":this.regForm.value["mobile"]};
this.commonService.postData(this.updateClientUrl,respObj).subscribe(
      res => {
        this.showTable = true;
       this.showUpdateButton = false;
this.showAddButton= true;
        if(res.status == 'success'){
          window.alert("Client Updated Successfully")
          this.fetchAllClients();
        }
        else{
          window.alert("Some problem occured while client update")
        }
      });

}

}


