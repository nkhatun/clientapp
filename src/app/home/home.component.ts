import { Component, OnInit } from '@angular/core';
import { ResponsiveService } from "../responsive.service";
import { CommonService } from "../common.service";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public isMobile: boolean;
  categoryUrl: string;
  catDataArray: any;
  subcatDataArray: any[] = [];
  responseMessage: string;
  isCollapsed = true;
  selectedPath: string;
  noData: boolean = false;

  constructor(private responsiveService: ResponsiveService, public commonService: CommonService) {
    this.categoryUrl = "http://208.109.13.111:9090/api/Category";
  }

  ngOnInit() {
    this.responsiveService.checkWidth();
    this.responsiveService.checkPath();
    this.onResize();
    this.loadCategoryDetails();
  }

  onResize() {
    this.responsiveService.getMobileStatus().subscribe(isMobile => {
      this.isMobile = isMobile;
    });
  }

  loadCategoryDetails() {
    this.commonService.getData(this.categoryUrl).subscribe(
      res => {
        this.catDataArray = res.result;
        if (this.catDataArray == null || this.catDataArray.length == 0) {
          this.catDataArray = [];
          this.noData = true;
        }
        else {
          this.noData = false;
          for (var i = 0; i < this.catDataArray.length; i++) {
            this.catDataArray[i].isCollapsed = 'false';
          }
        }
      });
  }
  
  toggle(index) {
    if (this.catDataArray[index].isCollapsed == 'true') {
      this.catDataArray[index].isCollapsed = 'false';
    } else {
      this.catDataArray[index].isCollapsed = 'true';
    }
  }


}
