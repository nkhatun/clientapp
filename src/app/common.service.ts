import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse ,HttpHeaders} from '@angular/common/http';
import { Observable,  throwError  } from 'rxjs';
import { map,  catchError, retry, tap, finalize  } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private http: HttpClient) { }

  public getData(url): Observable<any>  {
    return this.http.get(url)
          .pipe(
              catchError(this.errorHandler)
            );
   }

  public postData(url:string,data:any): Observable<any>  {
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json');
    headers.set('Access-Control-Allow-Origin','*');
    return this.http.post(url,data, {headers: headers})
          .pipe(
              catchError(this.errorHandler)
            );
   }

  errorHandler(error: HttpErrorResponse) {
    return  throwError(error);
  }

}
