import { Injectable } from '@angular/core';
import { Subject ,BehaviorSubject,Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ResponsiveService {

    private isMobile = new Subject();
    public screenWidth: string;
    public selctedPathUrl: string;
    private isHomeSelected = new Subject();


    constructor() {
        this.checkWidth();
        this.checkPath();
    }
    checkPath(){
        if(window.location.href.indexOf('home') != -1 || window.location.href.indexOf('information') != -1 ){
            this.isHomeSelected.next(true);
        }
        else{
            this.isHomeSelected.next(false);
        }        
    }
    getSelectedPath(): Observable<any> {
        return this.isHomeSelected.asObservable();
    }

    onMobileChange(status: boolean) {
        this.isMobile.next(status);
    }

    getMobileStatus(): Observable<any> {
        return this.isMobile.asObservable();
    }

    public checkWidth() {
        var width = window.innerWidth;
        if (width <= 768) {
            this.screenWidth = 'sm';
            this.onMobileChange(true);
        } else if (width > 768 && width <= 992) {
            this.screenWidth = 'md';
            this.onMobileChange(false);
        } else {
            this.screenWidth = 'lg';
            this.onMobileChange(false);
        }
    }
}
