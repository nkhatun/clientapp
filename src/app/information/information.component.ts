import { Component, OnInit } from '@angular/core';
import { ResponsiveService } from "../responsive.service";


@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit {
  showApp: boolean = false;
  bookApp: boolean = false;
  reachOption: string;
  public isMobile: boolean;

  constructor(private responsiveService: ResponsiveService) { }

  ngOnInit(): void {
    this.responsiveService.checkPath();
    this.onResize();
  }

  onResize() {
    this.responsiveService.getMobileStatus().subscribe(isMobile => {
      this.isMobile = isMobile;
    });
  }

  onReachOption(event) {
    if (event != null && event.value == '1') {
      this.showApp = true;
    }
    else {
      this.showApp = false;
    }
    this.bookApp = false;
  }
  
  onAppointment(event) {
    if (event != null) {
      this.bookApp = true;
    }
    else {
      this.bookApp = false;
    }
  }

}
